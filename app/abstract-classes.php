<?php 
abstract class AbstractClass{
	abstract protected function getValue();
	abstract protected function prefixValue($prefix);

	// Common Method
	public function printOut() {
		print $this->getValue();
	}
}

class ConcreteClass1 extends AbstractClass{
	public function getValue(){
		return 'ConcreteClass1 <br/>';
	}
	public function prefixValue($prefix){
		return "{$prefix}ConcreteClass1 <br/>";
	}
}

class ConcreteClass2 extends AbstractClass{
	public function getValue(){
		return 'ConcreteClass2 <br/>';
	}
	public function prefixValue($prefix){
		return "{$prefix}ConcreteClass2 </br>";
	}
}

$class1 = new ConcreteClass1();
$class1->printOut();
echo $class1->prefixValue("FOO_");

$class2 = new ConcreteClass2();
$class2->printOut();
echo $class2->prefixValue("FOO_");