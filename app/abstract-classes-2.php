<?php 
abstract class AbstractClass {
	// Abstract methods only need to define the required parameters
	abstract protected function prefixName($name);
}

class ConcreteClass extends AbstractClass{
	// Optional parameters could be added in the inherited function
	public function prefixName($name, $seperator = "."){
		if ($name == "Pacman") {
			$prefix = "Mr";
		} elseif ($name == "Pacwoman") {
			$prefix = "Mrs";
		} else {
			$prefix = "";
		}

		return "{$prefix}{$seperator} {$name}";
	}
}

$class = new ConcreteClass();
echo $class->prefixName("Pacman")."</br>";
echo $class->prefixName("Pacwoman")."</br>";