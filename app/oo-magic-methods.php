<?php 

class Hello {
	private $var = "twenty";

	public function __get($name){
		return $this->var;
	}
}

$o = new Hello();
echo $o->var;