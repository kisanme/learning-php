<?php 

/**
* 
*/
class BaseClass
{
	
	function __construct()
	{
		print("In BaseClass Constructor\n");
		echo ("<br/>");
	}
}

class SubClass extends BaseClass
{
	
	function __construct()
	{
		parent::__construct();
		print("In SubClass Constructor\n");
		echo ("<br/>");
	}
}

class OtherSubClass extends BaseClass
{
	// Inherits baseclass constructor
}

$obj = new BaseClass();
$obj = new SubClass();
$obj = new OtherSubClass();


// Class with constructor and destructor //
class DestructableClass {
	function __construct(){
		print("This is inside Constructor");
		echo "</br>";
	}

	function __destruct(){
		print("This is inside Destructor");
		echo "</br>";
	}
}

class SubDestClass extends DestructableClass {
	function __destruct(){
		parent::__destruct();
		print("Inside SubDestClass and inside Destruct function");
	}
}

$obj = new DestructableClass();
$obj = new SubDestClass();