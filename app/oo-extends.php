<?php 

class SimpleClass {
    public $var = "THIS IS A VARIABLE";

    public function displayVar(){
        echo $this->var;
    }
}

class ExtendClass extends SimpleClass {
	/// Redefining the parent method
	function displayVar(){
		echo "Extending class\n";
		parent::displayVar();
	}
}

$extended = new ExtendClass();
$extended->displayVar();