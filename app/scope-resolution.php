<?php 
//  :: from outside the class definition
class MyClassTest{
	const CONST_VALUE = 'A Constant value';
}

$classname = 'MyClassTest';
// echo $classname::CONST_VALUE;

// echo MyClassTest::CONST_VALUE;

// :: From outside the class definition
class OtherClassTest extends MyClass {
	public static $my_static = 'static var';

	public static function doubleColon(){
		echo parent::CONST_VALUE. "\n";
		echo '<br/>';
		echo self::$my_static . "\n";
		echo '<br/>';
	}
}

$classname = 'OtherClassTest';
// echo $classname::doubleColon(); 

// OtherClassTest::doubleColon();

// :: calling a parent's method
class MyClass {
	protected function myFunc() {
		echo "MyClass::myFunc()";
		echo '<br/>';
	}
}

class OtherClass extends MyClass {
	// Override parent's definition
	public function myFunc(){
		// But still call the parent function
		parent::myFunc();
		echo "OtherClass::myFunc()";
		echo '<br/>';
	}
}

$class = new OtherClass();
$class->myFunc();