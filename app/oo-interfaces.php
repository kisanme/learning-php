<?php 

// Interfaces example
interface iTemplate{
	public function setVariable($name, $var);
	public function getHTML($template);
}

class Template implements iTemplate{
	public $vars = array();

	public function setVariable($name, $var){
		$this->vars[$name] = $var;
	}

	public function getHTML($template){
		foreach ($this->vars as $name => $value){
			str_replace('{'.$name.'}', $value, $template);
		}
		return $template;
	}
}

$object = new Template();
$object->setVariable("h1", "HELLO THIS IS A TEST");
print($object->getHTML("{div}"));

// Extendeble interfaces
interface a {
	public function foo();
}

interface b extends a {
	public function baz(Baz $baz);
}

class c implements b {
	public function foo(){

	}
	public function baz(Baz $baz){

	}
}