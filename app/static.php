<?php 
// Static property example
class Foo {
	public static $mystatic = "foo";

	public function staticValue(){
		return self::$mystatic;
	}
}

class Bar extends Foo {
	public function fooStatic(){
		return parent::$mystatic;
	}
}

print Foo::$mystatic . "<br/>";

$foo = new Foo();
print $foo->staticValue() . '<br/>';

print $foo::$mystatic. "<br/>";

print Bar::$mystatic. "<br/>";
$bar = new Bar();
print $bar->fooStatic(). "<br/>";

// Static method example
class Soo {
	public static function aStaticMethod(){
		print "This is a static method <br/>";
	}
}

Soo::aStaticMethod();
$classname = "Soo";
$classname::aStaticMethod();