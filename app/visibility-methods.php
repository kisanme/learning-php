<?php 

class MyClass 
{
	// Declare a public constructor
	public function __construct(){}

	// Declare a public Function
	public function myPublic(){}

	// Declare a protected function
	protected function myProtected(){}

	// Declare a private function
	private function myPrivate(){}

	// This is public function
	function foo(){
		$this->myPublic();
		$this->myProtected();
		$this->myPrivate();
	}
}

$myclass = new MyClass();
$myclass->myPublic();
// Following functions cannot be called outside of the class
// $myclass->myProtected();
// $myclass->myPrivate();
$myclass->foo();

class SubClass extends MyClass{
	// This is public function
	function foo2(){
		$this->myPublic();
		$this->myProtected();
		// Private function does not work from an inherited class
		// $this->myPrivate();
	}
}

$subclass = new SubClass();
$subclass->myPublic();
$subclass->foo2();

class Bar
{
	public function test(){
		$this->testPrivate();
		$this->testPublic();
	}

	public function testPublic(){
		echo "Bar::testPublic";
		echo "</br>";
	}

	private function testPrivate(){
		echo "Bar::testPrivate";
		echo "</br>";
	}
}

class Foo extends Bar 
{
	public function testPublic(){
		echo "Foo::testPublic";
		echo "</br>";
	}

	private function testPrivate(){
		echo "Foo::testPrivate";
	}
}

$myFoo = new foo();
$myFoo->test();

// Visibility from other objects
class Test 
{
	private $foo;

	public function __construct($foo){
		$this->foo = $foo;
	}

	private function bar(){
		echo "Accessed the private Method.";
	}

	public function baz(Test $other){
		// we can change the private property
		$other->foo = 'Hello';
		var_dump($other->foo);

		// We can also call the private method
		$other->bar();
	}
}

$test = new Test('test');
$test->baz(new Test('other'));