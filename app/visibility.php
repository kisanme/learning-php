<?php 

/**
* 
*/
class MyClass
{
	public $public = "Public";
	protected $protected = "Protected";
	private $private = "Private";

	function printHello(){
		echo $this->public."</br>";
		echo $this->protected."</br>";
		echo $this->private."</br>";
	}
}

$obj = new MyClass();
echo $obj->public."</br>";
// Does not print the following
// echo $obj->protected;
// echo $obj->private;
$obj->printHello();

class SubClass extends MyClass
{
	protected $protected = "protected2";
	// protected $private = "Private2";
	function printHello(){
		echo $this->public."</br>";
		echo $this->protected."</br>";
		echo $this->private."</br>";	// This will be not inherited from the parent
	}
}

$object = new SubClass();
echo $object->public."</br>";
// Does not print the following
// echo $object->protected;
// echo $object->private;
$object->printHello();